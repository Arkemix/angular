import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DestinoViajeComponent } from '../../src/app/components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from '../../src/app/components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from '../../src/app/components/destino-detalle/destino-detalle.component';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosDetalleComponentComponent } from './components/vuelo/vuelos-detalle-component/vuelos-detalle-component.component';
import { VuelosMainComponentComponent } from './components/vuelo/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelo/vuelos-mas-info-component/vuelos-mas-info-component.component';
import {VuelosComponentComponent } from './components/vuelo/vuelos-component/vuelos-component.component';

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentComponent },
  { path: ':id', component: VuelosDetalleComponentComponent },
];
const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: ListaDestinosComponent},
  {path: 'destino/:id', component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
