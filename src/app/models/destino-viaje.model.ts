import {v4 as uuid} from 'uuid';
export class DestinoViaje {
    id = uuid();
    selected: boolean;
    servicios: string[];
    constructor(public n: string, public u: string, public votes: number = 0){
        this.servicios = ['pileta', 'desayuno'];
    }

    isSelected(): boolean{
        return this.selected;
    }
    // tslint:disable-next-line: typedef
    setSelected(s: boolean) {
        this.selected = s;
    }

    voteUp(): any {
        this.votes++;
    }
    voteDown(): any {
        this.votes--;
    }
}
